#!/bin/zsh

brew upgrade
mas upgrade 
brew cu --all -y
# composer global upgrade
# pecl upgrade msgpack
# pip3 install neovim --upgrade
# npm update -g
sudo tlmgr update --self --all
sudo R -e "update.packages(ask = FALSE, repos = 'https://cloud.r-project.org/', Ncpus = 5)"
