# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

export TERM="xterm-256color"

LANG="de_DE.UTF-8"
LC_COLLATE="de_DE.UTF-8"
LC_CTYPE="de_DE.UTF-8"
LC_MESSAGES="de_DE.UTF-8"
LC_MONETARY="de_DE.UTF-8"
LC_NUMERIC="de_DE.UTF-8"
LC_TIME="de_DE.UTF-8"
LC_ALL="de_DE.UTF-8"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME=""

# Use Starship Prompt
eval "$(starship init zsh)"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    macos
    git
    zsh-autosuggestions
    zsh-vi-mode
    eza
)

source $ZSH/oh-my-zsh.sh

# Autosuggestions config
ZSH_AUTOSUGGEST_STRATEGY=(history completion)

# User configuration
if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

export GPG_TTY=$(tty)

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# DEVELOPMENT
alias python='python3'
alias jigsaw='vendor/bin/jigsaw'
alias pip='pip3'
alias R="radian"
alias c='clear'
alias skim='open -a Skim.app'

alias vim=nvim

alias update='sh ~/Repositories/dotfiles/update.sh'

export PATH="/usr/local/sbin:$PATH"

export PATH="$HOME/.composer/vendor/bin:$PATH"

export PATH="/Library/Frameworks/R.framework/Versions/Current/Resources:$PATH"

export PATH="$PATH:/Applications/Docker.app/Contents/Resources/bin/"

# Media keys
alias mp='music play'
alias mn='music next'
alias mb='music previous'
alias ms='music pause'

# R
alias build_r_package="/Library/Frameworks/R.framework/Versions/Current/Resources/R CMD INSTALL --no-multiarch --with-keep.source ."

alias build_r_package_clean="/Library/Frameworks/R.framework/Versions/Current/Resources/R CMD INSTALL --preclean --no-multiarch --with-keep.source ."

# Shrink PDF
alias shrink_pdf="sh $HOME/Repositories/dotfiles/shrinkpdf.sh"

# Certbot
function certbot-renew() {
    sudo certbot certonly --manual -d www.$1 -d $1 --agree-tos --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory --register-unsafely-without-email --rsa-key-size 4096
    sudo cp /etc/letsencrypt/live/www.$1/fullchain.pem $HOME/Desktop/fullchain.pem
    sudo cp /etc/letsencrypt/live/www.$1/privkey.pem $HOME/Desktop/privkey.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/fullchain.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/privkey.pem
}

function certbot-renew-subdomain {
    sudo certbot certonly --manual -d $1 --agree-tos --preferred-challenges dns-01 --server https://acme-v02.api.letsencrypt.org/directory --register-unsafely-without-email --rsa-key-size 4096
    sudo cp /etc/letsencrypt/live/$1/fullchain.pem $HOME/Desktop/fullchain.pem
    sudo cp /etc/letsencrypt/live/$1/privkey.pem $HOME/Desktop/privkey.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/fullchain.pem
    sudo chown fabianmundt:everyone $HOME/Desktop/privkey.pem
}

# Add the function to the environment variable in either Zsh or Bash
if [ -n "$ZSH_VERSION" ]; then
  precmd_functions+=(set_name)
elif [ -n "$BASH_VERSION" ]; then
  PROMPT_COMMAND='set_name'
fi

# ZSH Syntax Highlighting
source $(brew --prefix)/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# ZSH Substring history search
source $(brew --prefix)/share/zsh-history-substring-search/zsh-history-substring-search.zsh

bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

eval $(thefuck --alias)

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Sail alias
alias sail='sh $([ -f sail ] && echo sail || echo vendor/bin/sail)'

# Created by `pipx` on 2024-09-18 15:04:24
export PATH="$PATH:/Users/fabianmundt/.local/bin"
