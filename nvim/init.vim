" ███╗   ██╗ ███████╗ ██████╗  ██╗   ██╗ ██╗ ███╗   ███╗
" ████╗  ██║ ██╔════╝██╔═══██╗ ██║   ██║ ██║ ████╗ ████║
" ██╔██╗ ██║ █████╗  ██║   ██║ ██║   ██║ ██║ ██╔████╔██║
" ██║╚██╗██║ ██╔══╝  ██║   ██║ ╚██╗ ██╔╝ ██║ ██║╚██╔╝██║
" ██║ ╚████║ ███████╗╚██████╔╝  ╚████╔╝  ██║ ██║ ╚═╝ ██║
" ╚═╝  ╚═══╝ ╚══════╝ ╚═════╝    ╚═══╝   ╚═╝ ╚═╝     ╚═╝
"
" Author: Fabian Mundt (Inventionate)
" Repository: https://gitlab.com/Inventionate/dotfiles
"
" ----------------------------------------------------------------------------
" OPTIONS
" ----------------------------------------------------------------------------

" Carry over indenting from previous line
set autoindent
" Use smart indention
set smartindent
" Allow backspace beyond insertion point
set backspace=indent,eol,start
" Automatic program indenting
set cindent
" Comments don't fiddle with indenting
set cinkeys-=0#
" See :h cinoptions-values
set cinoptions=
" When folds are created, add them to this
set commentstring=\ \ #%s
" UTF-8 by default
set encoding=utf-8
" UTF-8 by default
scriptencoding utf-8
" No tabs
set expandtab
" Prefer Unix
set fileformats=unix,dos,mac
" Unicode chars for diffs/folds, and rely on Colors for window borders
set fillchars=vert:\ ,stl:\ ,stlnc:\ ,fold:-,diff:┄
" Use braces by default
set foldmethod=marker
" 120 is the new 80
set textwidth=120
" Format text
set formatoptions=tcqn1o
" Show wrap line
set colorcolumn=+1
" How many lines of history to save
set history=1000
" Hilight searching
set hlsearch
" Case insensitive
set ignorecase
" live search substitution
set inccommand=nosplit
" Search as you type
set incsearch
" Completion recognizes capitalization
set infercase
" Use 24bit colours.
set termguicolors
" We'll fake a custom left padding for each window.
set foldcolumn=1
" Always show the status bar
set laststatus=3
" Break long lines by word, not char
set linebreak
" Show whitespace as special chars - see listchars
set list
" Unicode characters for various things
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:¶,trail:·
" Tenths of second to hilight matching paren
set matchtime=2
" How many lines of head & tail to look for ml's
set modelines=5
" No line numbers to start
set number
" Signs in number column
set signcolumn=number
" No flashing or beeping at all
set visualbell t_vb=
" Hide row/col and percentage
set noruler
" Hise command line infos
set noshowcmd
" Number of lines to scroll with ^U/^D
set scroll=4
" Keep cursor away from this many chars top/bot
set scrolloff=15
" Enable mouse support
set mouse=a
" Don't save runtimepath in Vim session (see tpope/vim-pathogen docs)
set sessionoptions-=options
" Shift to certain columns, not just n spaces
set shiftround
" Number of spaces to shift for autoindent or >,<
set shiftwidth=4
" Hide Omnicomplete messages
set shortmess+=c
" Show for lines that have been wrapped, like Emacs
set showbreak=
" Showmatch Hilight matching braces/parens/etc.
set showmatch
" Turn off show mode (showed by lightline)
set noshowmode
" Keep cursor away from this many chars left/right
set sidescrolloff=3
" Lets you search for ALL CAPS
set smartcase
" Spaces 'feel' like tabs
set softtabstop=4
" Ignore these files when tab-completing
set suffixes+=.pyc
" The One True Tab
set tabstop=4
" Don't set the title of the Vim window
set notitle
" Disable conceal
set conceallevel=0
" Disable omnicompletion preview
set completeopt=menuone,noselect,noinsert
" Ignore certain files in tab-completion
set wildignore=*.class,*.o,*~,*.pyc,.git,.DS_Store
" Make splits defaut to below ...
set splitbelow
" ... and to the right
set splitright
" Always hide tabline
set showtabline=0
" Essential for filetype plugins
filetype plugin indent on
" Make sure the original file is overwritten on save
set backupcopy=yes
" Backup file dir
set backupdir=$HOME/iCloud/Vim/backup
" Swap file dir
set directory=$HOME/iCloud/Vim/swap
" Enable persistent undo
set undofile
" Undo file dir
set undodir=$HOME/iCloud/Vim/undo
" Turn off modeline support
set nomodeline
" Set update time
set updatetime=1000
" Autosave
set autowriteall
" Nice split seperator
set fillchars=vert:▏,horiz:▁

" ----------------------------------------------------------------------------
" PLUGIN SETTINGS
" ----------------------------------------------------------------------------

" Load plugins
lua require('plugins')

" For any plugins that use this, make their keymappings use comma
let mapleader = ','
let maplocalleader = ';'

" Pear Tree
let g:pear_tree_ft_disabled = ['TelescopePrompt']

" Limelight
let g:limelight_conceal_guifg='#777777'

" Thesaurus
let g:tq_language = ['de', 'en']
let g:tq_map_keys = 0
let g:tq_use_vim_autocompletefunc = 0

" IndentLine
let g:indent_blankline_char = '│'
let g:indent_blankline_use_treesitter = v:true
let g:indent_blankline_show_current_context = v:true
let g:indent_blankline_filetype_exclude = ['help', 'dashboard', 'lspinfo', 'TelescopePrompt']

" RStudio like sections
function! s:fillLine( str )
    " set tw to the desired total length
    let tw = &textwidth - 40
    if tw==0 | let tw = 80 | endif
    " strip trailing spaces first
    .s/[[:space:]]*$//
    " calculate total number of 'str's to insert
    let reps = (tw - col("$")) / len(a:str)
    " insert them, if there's room, removing trailing spaces (though forcing there to be one)
    if reps > 0
        .s/$/\=(' '.repeat(a:str, reps))/
    endif
endfunction

" Insert code chunk function
function! s:insertChunk()
    normal!a```{r}
    normal!o```
    normal!O
endfunction

augroup r_setup
autocmd!
    " Disable colorcolumn
    autocmd FileType markdown,rmd,quarto setlocal colorcolumn=0
    " Disable autowrap
    autocmd FileType markdown,rmd,quarto setlocal formatoptions-=t
    " Assigning
    autocmd FileType r,rmd,quarto inoremap <buffer> – <Space><-<Space>
    " Pipe operator
    autocmd FileType r,rmd,quarto inoremap <buffer> ≤≤ <Esc><cmd>normal! a<Space>\|><CR>a
    " Sections like RStudio
    autocmd FileType r inoremap <buffer> ## <esc><cmd>call <SID>fillLine( '-' )<CR>o<C-U><CR>
    autocmd FileType r inoremap <buffer> *** <esc><cmd>call <SID>fillLine( '*' )<CR>o<C-U>
    " Insert Code Chunks
    autocmd FileType rmd,quarto inoremap <buffer> >> <cmd>call <SID>insertChunk()<CR>
    " Set REPL interpreter
    autocmd FileType r,rmd,quarto
          \ if executable(g:neoterm_repl_r) |
          \   call neoterm#repl#set(g:neoterm_repl_r) |
          \ elseif executable('R') |
          \   call neoterm#repl#set('R') |
          \ end
    " Neoterm for radian
    " General run command (combine it with text-objects)
    autocmd FileType r,rmd,quarto nmap s <Plug>(neoterm-repl-send)
    " Run current selection
    autocmd FileType r,rmd,quarto vmap <space>r <esc><cmd>TREPLSendSelection<CR>
    " Run the current line
    autocmd FileType r,rmd,quarto nmap <space>r <cmd>TREPLSendLine<CR>
    " Run current pipe
    autocmd FileType r,rmd,quarto nmap <space>p sip}<CR>
    " Send code chunk
    autocmd FileType rmd,quarto nmap <space>s sic<CR>
    " Run the whole file
    autocmd FileType r,rmd,quarto nmap <space>f s%
    " Close console
    autocmd FileType r,rmd,quarto nmap <space>c <cmd>TcloseAll!<CR>
    " Toggle console
    autocmd FileType r,rmd,quarto nmap <space>, <cmd>Ttoggle<CR>
augroup END

" Dashboard
augroup dashboard_setup
    autocmd!
    autocmd FileType dashboard setlocal scrolloff=0
augroup END

" Remove all trailing whitespaces
function! s:stripTrailingWhitespaces()
    if !&binary && &filetype != 'diff'
        let l:save = winsaveview()
        keeppatterns %s/\s\+$//e
        call winrestview(l:save)
    endif
endfun

augroup strip_trailing_whitespaces
    autocmd!
    autocmd FileType css,php,r,vim autocmd BufWritePre <buffer> call <SID>stripTrailingWhitespaces()
augroup END

" Vim Rooter
let g:rooter_change_directory_for_non_project_files = 'current'
let g:rooter_patterns = ['.git', 'build.sh']
let g:rooter_silent_chdir = 1

" Highlight yank
au TextYankPost * silent! lua vim.highlight.on_yank {higroup="HighlightedyankRegion", timeout=250, on_visual=true}

" ----------------------------------------------------------------------------
" COLORS
" ----------------------------------------------------------------------------

let g:lumen_dark_colorscheme = 'dracula_pro'
let g:lumen_light_colorscheme = 'dracula_pro_alucard'
let g:lumen_startup_overwrite = 0

function! s:alucardInventionate() abort
    " Change lualine theme
    lua require('lualine').setup{options = {theme = require('lualine-themes.dracula_pro_light')}}
    " Dashboard
    highlight DashboardHeader guifg=#036A96
    highlight DashboardKey guifg=#A3144D
    highlight DashboardIcon guifg=#644AC9
    highlight DashboardDesc guifg=#1F1F1F
    " Hide numbers
    highlight LineNr guifg=#F5F5F5
    " Telescope
    highlight TelescopePromptBorder guifg=#635D97
    highlight TelescopeResultsBorder guifg=#635D97
    highlight TelescopePreviewBorder guifg=#635D97
    highlight TelescopeSelection guifg=#644AC9
    highlight TelescopeNormal guifg=#1F1F1F guibg=#F5F5F5
    highlight TelescopeMatching guifg=#14710A
    highlight TelescopePromptPrefix guifg=#644AC9
    highlight TelescopeResultsDiffDelete guifg=#CB3A2A
    highlight TelescopeResultsDiffChange guifg=#036A96
    highlight TelescopeResultsDiffAdd guifg=#14710A
    " Lazy
    highlight LazyButton guibg=#F5F5F5
    highlight LazyNormal guibg=#F5F5F5
    highlight FloatBorder guibg=#F5F5F5 guifg=#635D97
    " Markdown
    highlight @markup.link.label guifg=#635D97
    highlight @markup.link.url guifg=#8AFF80
    highlight @markup.italic gui=italic guifg=#FFFF80
    highlight @markup.strong gui=bold guifg=#FFCA80
    highlight @markup.raw guifg=#FF79C6
    " Distinguishing terminal mode cursor
    highlight! link TermCursor Cursor
    highlight! TermCursorNC guibg=#FF9580 guifg=#FFFFFF
    " Improve delete colours for Gitsign
    highlight! GitSignsDelete gui=NONE guifg=#FF9580 guibg=#F5F5F5
    highlight! GitSignsUntracked gui=NONE guifg=#AAAAAA guibg=#F5F5F5
    " Highlightyank
    highlight HighlightedyankRegion guibg=#FFCA80 guifg=#F8F8F2
    " LSP Highlight
    highlight LocalHighlight guibg=#454158
    " Bulb Sign
    highlight! LspLightBulbSign guibg=#F5F5F5 guifg=#FFFF80
    " Split column
    highlight WinSeparator guibg=NONE
endfunction

function! s:draculaInventionate() abort
    " Change lualine theme
    lua require('lualine').setup{options = {theme = require('lualine-themes.dracula_pro_dark')}}
    " Dashboard
    highlight DashboardHeader guifg=#80FFEA
    highlight DashboardKey guifg=#FF80BF
    highlight DashboardIcon guifg=#9580FF
    highlight DashboardDesc guifg=#F8F8F2
    " Hide numbers
    highlight LineNr guifg=#22212C
    " Telescope
    highlight TelescopePromptBorder guifg=#7970A9
    highlight TelescopeResultsBorder guifg=#7970A9
    highlight TelescopePreviewBorder guifg=#7970A9
    highlight TelescopeSelection guifg=#9580FF
    highlight TelescopeNormal guifg=#F8F8F2 guibg=#22212C
    highlight TelescopeMatching guifg=#8AFF80
    highlight TelescopePromptPrefix guifg=#9580FF
    highlight TelescopeResultsDiffDelete guifg=#FF9580
    highlight TelescopeResultsDiffChange guifg=#80FFEA
    highlight TelescopeResultsDiffAdd guifg=#8AFF80
    " Lazy
    highlight LazyButton guibg=#22212C
    highlight LazyNormal guibg=#22212C
    highlight FloatBorder guibg=#22212C guifg=#7970A9
    " Markdown
    highlight @markup.link.label guifg=#7970A9
    highlight @markup.link.url guifg=#8AFF80
    highlight @markup.italic gui=italic guifg=#FFFF80
    highlight @markup.strong gui=bold guifg=#FFCA80
    highlight @markup.raw guifg=#FF79C6
    " Distinguishing terminal mode cursor
    highlight! link TermCursor Cursor
    highlight! TermCursorNC guibg=#FF9580 guifg=#FFFFFF
    " Improve delete colours for Gitsign
    highlight! GitSignsDelete gui=NONE guifg=#FF9580 guibg=#22212C
    highlight! GitSignsUntracked gui=NONE guifg=#AAAAAA guibg=#22212C
    " Highlightyank
    highlight HighlightedyankRegion guibg=#FFCA80 guifg=#F8F8F2
    " LSP Highlight
    highlight LocalHighlight guibg=#454158
    " Bulb Sign
    highlight! LspLightBulbSign guibg=#454158 guifg=#FFFF80
    " Split column
    highlight WinSeparator guibg=NONE
endfunction

if &background ==# 'light'
    colorscheme dracula_pro_alucard
    call s:alucardInventionate()
elseif &background ==# 'dark'
    colorscheme dracula_pro
    call s:draculaInventionate()
endif

augroup dracula_inventionate
    autocmd!
    autocmd User LumenLight call s:alucardInventionate()
    autocmd User LumenDark call s:draculaInventionate()
augroup END

" ----------------------------------------------------------------------------
" KEY MAPS
" ----------------------------------------------------------------------------

" Make it easy to edit the Vimrc file.
nmap <Leader>ev <cmd>tabedit $MYVIMRC<CR>

" Turn off linewise keys. Normally, the `j' and `k' keys move the cursor down one entire line. with
" line wrapping on, this can cause the cursor to actually skip a few lines on the screen because
" it's moving from line N to line N+1 in the file. I want this to act more visually -- I want `down'
" to mean the next line on the screen
nmap j gj
nmap k gk

" Bufonly
nmap <silent> <leader>bd <cmd>BufOnly<CR>

" Zen Mode
nmap <silent> <leader>z <cmd>ZenMode<CR>

" Fuzzy finder Telescope
nmap <silent> <leader>f <cmd>Telescope find_files<CR>
nmap <silent> - <cmd>Telescope file_browser path=%:p:h select_buffer=true<CR>
nmap <silent> <leader><leader> <cmd>Telescope buffers<CR>
nmap <silent> <leader>s <cmd>Telescope live_grep<CR>
imap <silent> <leader>c <cmd>Telescope bibtex<CR>

" Exit Terminal mode by ESC
tnoremap <Esc> <C-\><C-n>

" Exit Shell program
tnoremap <C-v><Esc> <Esc>

" Thesaurus
nnoremap <leader>ct <cmd>ThesaurusQueryReplaceCurrentWord<CR>
vnoremap <leader>ct y<cmd>ThesaurusQueryReplace <C-r>"<CR>

" Copy to clipboard
vnoremap  <leader>y  "+y
nnoremap  <leader>Y  "+yg_
nnoremap  <leader>y  "+y

" Paste from clipboard
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

" Easy align
nmap ga <Plug>(EasyAlign)
xmap ga <Plug>(EasyAlign)

" Incremental search improvements
noremap <silent> n <Cmd>execute('normal! ' . v:count1 . 'n')<CR>
            \<Cmd>lua require('hlslens').start()<CR>
noremap <silent> N <Cmd>execute('normal! ' . v:count1 . 'N')<CR>
            \<Cmd>lua require('hlslens').start()<CR>
noremap * *<Cmd>lua require('hlslens').start()<CR>
noremap # #<Cmd>lua require('hlslens').start()<CR>
noremap g* g*<Cmd>lua require('hlslens').start()<CR>
noremap g# g#<Cmd>lua require('hlslens').start()<CR>
" use : instead of <Cmd>
nnoremap <silent> <leader>l :noh<CR>

" Delete all buffers
nmap <silent> << <cmd>BufDel<CR>

" Close Neovim
nmap <leader>q :qa!<CR>

" Undotree
nnoremap <F5> <cmd>MundoToggle<CR>

" Write file
nnoremap <space><space> <cmd>w<CR>
