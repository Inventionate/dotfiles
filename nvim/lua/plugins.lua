-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out, "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup({
    spec = {
        -- Theme
        {
            dir = '~/iCloud/Allgemein/Dracula Pro/themes/vim',
            lazy = false,
            priority = 1000,
        },
        -- Theme switcher
        'vimpostor/vim-lumen',
        -- Git integration
        'tpope/vim-fugitive',
        -- Terminal commands
        'tpope/vim-eunuch',
        -- Search and replace improvement
        'tpope/vim-abolish',
        -- Peeking the buffer while entering line number
        {
            'nacro90/numb.nvim',
            config = function()
                require('numb').setup()
            end
        },
        -- Which key
        {
            'folke/which-key.nvim',
            event = "VeryLazy",
        },
        -- Show colour columny dynamically
        { 'Bekaboo/deadcolumn.nvim' },
        -- Git status in gutter
        {
            'lewis6991/gitsigns.nvim',
            dependencies = {
                'nvim-lua/plenary.nvim'
            },
            config = function()
                require('config.gitsigns')
            end
        },
        -- Startscreen
        {
            'glepnir/dashboard-nvim',
            event = 'VimEnter',
            config = function()
                require('config.dashboard')
            end
        },
        -- Improved search
        {
            'kevinhwang91/nvim-hlslens',
            config = function()
                require('hlslens').setup({
                    calm_down = true,
                })
            end
        },
        -- Search and replace
        {
            "chrisgrieser/nvim-rip-substitute",
            cmd = "RipSubstitute",
        },
        -- Status bar
        {
            'nvim-lualine/lualine.nvim',
            config = function()
                require('config.lualine')
            end
        },
        -- LSP lualine support
        {
            'pnx/lualine-lsp-status'
        },
        -- Improved alignments
        'junegunn/vim-easy-align',
        -- Dev icon support
        'kyazdani42/nvim-web-devicons',
        -- Fast navigation
        {
            'ggandor/leap.nvim',
            config = function()
                require('leap').add_default_mappings()
            end
        },
        -- Close all hidden buffers
        'schickling/vim-bufonly',
        -- Improved buffer close
        'ojroques/nvim-bufdel',
        -- Fuzzy search
        {
            'nvim-telescope/telescope.nvim',
            branch = '0.1.x',
            config = function()
                require('config.telescope')
            end,
            dependencies = {
                'nvim-lua/plenary.nvim',
                'nvim-telescope/telescope-bibtex.nvim',
                "debugloop/telescope-undo.nvim",
            }
        },
        -- Telescope FZF
        {
            'nvim-telescope/telescope-fzf-native.nvim',
            build = 'make'
        },
        -- File browser
        'nvim-telescope/telescope-file-browser.nvim',
        -- Repo browser
        'cljoly/telescope-repo.nvim',
        -- Language Server support
        {
            'williamboman/mason.nvim',
            config = function()
                require("mason").setup()
            end
        },
        {
            'williamboman/mason-lspconfig.nvim',
            config = function()
                require('mason-lspconfig').setup {
                    automatic_installation = true,
                    ensure_installed = {
                        'cssls',
                        'html',
                        'jsonls',
                        'ltex',
                        'texlab',
                        'lua_ls',
                        'marksman',
                        'intelephense',
                        'r_language_server',
                        'vimls'
                    },
                }
            end
        },
        {
            'neovim/nvim-lspconfig',
            dependencies = {
                -- Info about Code Actions
                {
                    'kosayoda/nvim-lightbulb',
                    config = function()
                        require('nvim-lightbulb').setup({ autocmd = { enabled = true } })
                    end
                },
                -- 'hrsh7th/nvim-cmp',
                -- 'hrsh7th/cmp-nvim-lsp',
            },
            config = function()
                require('config.lsp')
            end
        },
        -- Better highlight
        {
            'tzachar/local-highlight.nvim',
            config = function()
                require('local-highlight').setup({
                    file_types = { 'r', 'rmd', 'quarto', 'php', 'vue' },
                })
            end
        },
        {
            "rachartier/tiny-inline-diagnostic.nvim",
            event = "VeryLazy",
            config = function()
                require('tiny-inline-diagnostic').setup()
            end
        },
        -- Auto pairs
        'tmsvg/pear-tree',
        -- Colour output
        {
            'NvChad/nvim-colorizer.lua',
            config = function()
                require('colorizer').setup()
            end
        },
        {
            'folke/todo-comments.nvim',
            dependencies = 'nvim-lua/plenary.nvim',
            config = function()
                require('todo-comments').setup {}
            end
        },
        -- Mark line indent
        'lukas-reineke/indent-blankline.nvim',
        -- Treesitter
        {
            'nvim-treesitter/nvim-treesitter',
            build = ':TSUpdate',
            event = { "BufReadPre", "BufNewFile" },
            dependencies = {
                -- Treesitter context
                'romgrk/nvim-treesitter-context',
                'nvim-treesitter/nvim-treesitter-textobjects',
            },
            config = function()
                require('config.treesitter')
            end
        },
        -- Automatic project wd switch
        'airblade/vim-rooter',
        -- Smooth scrolling
        {
            'declancm/cinnamon.nvim',
            config = function()
                require('cinnamon').setup()
            end
        },
        -- Zen mode
        {
            'folke/zen-mode.nvim',
            config = function()
                require('config.zen-mode')
            end,
            dependencies = {
                -- Zen focus addon
                'folke/twilight.nvim'
            },
        },
        -- LanguageTool
        {
            'rhysd/vim-grammarous',
            ft = { 'rmd', 'text', 'pandoc', 'tmd', 'markdown', 'quarto' }
        },
        -- LTeX extra (das hier noch mal genau checken!)
        {
            'barreiroleo/ltex-extra.nvim',
            ft = { 'bib', 'markdown', 'org', 'plaintex', 'rst', 'rnoweb', 'tex', 'rmd', 'quarto' },
        },
        -- Thesaurus
        {
            'ron89/thesaurus_query.vim',
            ft = { 'rmd', 'text', 'pandoc', 'tmd', 'markdown', 'quarto' }
        },
        -- REPL
        {
            'kassio/neoterm',
            config = function()
                -- Enable bracketed paste
                vim.g.neoterm_bracketed_paste = 1

                -- Default R REPL
                vim.g.neoterm_repl_r = 'radian'

                -- Don't add extra call to REPL when sending
                vim.g.neoterm_direct_open_repl = 1

                -- Open terminal to the right by default
                vim.g.neoterm_default_mod = 'vertical'

                -- Size of the console
                vim.g.neoterm_size = 40

                -- Scroll to recent command when it is executed
                vim.g.neoterm_autoscroll = 1

                -- Don't automap keys
                vim.keymap.del('n', ',tt')

                -- Change default shell to zsh (if it is installed)
                if vim.fn.executable('zsh') == 1 then vim.g.neoterm_shell = 'zsh' end
            end
        },
        {
            "HakonHarnes/img-clip.nvim",
            event = "BufEnter",
            opts = {
                filetypes = {
                    quarto = {
                        template = "![$CURSOR]($FILE_PATH)",
                        dir_path = "images",
                        use_absolute_path = false,
                        url_encode_path = true,
                        drag_and_drop = {
                            enabled = true,
                            insert_mode = true,
                            download_images = true,
                            copy_images = true,
                        },
                    },
                },
            },
            keys = {
                -- suggested keymap
                { "<leader>i", "<cmd>PasteImage<cr>", desc = "Paste clipboard image" },
            },
        },
    },
    ui = {
        border = 'rounded',
        backdrop = 100,
        pills = false,
    },
    install = {
        colorscheme = { 'dracula' },
    },
})
