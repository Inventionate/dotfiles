require("telescope").setup {
    defaults = {
        layout_strategy = 'center',
        borderchars = {
            prompt = { "─", "│", " ", "│", "╭", "╮", "│", "│" },
            results = { "─", "│", "─", "│", "├", "┤", "╯", "╰" },
            preview = { "─", "│", "─", "│", "╭", "╮", "╯", "╰" },
        },
        results_title = false,
        prompt_prefix = '❯ ',
        selection_caret = '❯ ',
        selection_strategy = 'reset',
        sorting_strategy = 'ascending',
        scroll_strategy = 'cycle',
        color_devicons = true,

    },
    pickers = {
        buffers = {
            ignore_current_buffer = true,
            sort_lastused = true,
        },
    },
    extensions = {
        repo = {
            list = {
                fd_opts = {
                    "--no-ignore-vcs",
                },
                search_dirs = {
                    "~/Repositories/",
                },
            },
        },
        bibtex = {
            global_files = {"~/iCloud/Documents/Papers/Bibliography.bib"},
        },
    },
}

require('telescope').load_extension('fzf')
require("telescope").load_extension "file_browser"
require("telescope").load_extension "repo"
require("telescope").load_extension "bibtex"
require("telescope").load_extension("undo")
