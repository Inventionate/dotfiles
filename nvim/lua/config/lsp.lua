local lspconfig = require('lspconfig')
local configs = require('lspconfig.configs')

local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
    -- Enable omnifunc in general
    buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
    -- Diagnostic Signs
    vim.fn.sign_define("DiagnosticSignError",
        {text = "●", texthl = "DiagnosticSignError", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignWarn",
        {text = "●", texthl = "DiagnosticSignWarn", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignInfo",
        {text = "●", texthl = "DiagnosticSignInfo", linehl = "", numhl = ""})
    vim.fn.sign_define("DiagnosticSignHint",
        {text = "●", texthl = "DiagnosticSignHint", linehl = "", numhl = ""})
    -- Mappings
    local opts = { noremap=true, silent=true }
    buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
    buf_set_keymap('n', '<leader>D', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
    buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
    buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
    buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
    buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
    buf_set_keymap('n', '<leader>r', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
    buf_set_keymap('n', '<space>e', '<cmd>lua vim.diagnostic.open_float({scope = "cursor"})<CR>', opts)
    buf_set_keymap('n', '<space>n', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
    buf_set_keymap('n', '<space>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)
    -- Set some keybinds conditional on server capabilities
    -- if client.server_capabilities.documentFormattingProvider then
    --     buf_set_keymap("n", "ff", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
    -- end
    -- if client.server_capabilities.documentRangeFormattingProvider then
    --     buf_set_keymap("v", "ff", "<cmd>lua vim.lsp.buf.format()<CR>", opts)
    -- end
    -- LTeX extra
    if client.name == "ltex" then
        require("ltex_extra").setup{
            load_langs = { "de-DE", "en-GB" }, -- table <string> : languages for witch dictionaries will be loaded
            init_check = true, -- boolean : whether to load dictionaries on startup
            path = "/Users/fabianmundt/iCloud/Vim/ltex/", -- string : path to store dictionaries. Relative path uses current working directory
        }
    end
end

lspconfig.html.setup {
    on_attach = on_attach,
    filetypes = { "html", "blade" }
}

lspconfig.intelephense.setup {
    on_attach = on_attach,
    init_options = {
        licenceKey = "/Users/fabianmundt/iCloud/Documents/Sicherheitscodes/intelephense.txt";
      }
}

lspconfig.cssls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        css = { validate = false },
        scss = { validate = false }
    }
}

lspconfig.r_language_server.setup {
    on_attach = on_attach,
    filetypes = { "r", "rmd", "quarto" },
}

lspconfig.marksman.setup {
    filetypes = { "markdown", "quarto" },
}

lspconfig.vimls.setup {
    on_attach = on_attach,
}

lspconfig.texlab.setup {
    on_attach = on_attach,
}

lspconfig.lua_ls.setup {
    on_attach = on_attach,
}

lspconfig.jsonls.setup {
    on_attach = on_attach,
}

lspconfig.ltex.setup {
    on_attach = on_attach,
    filetypes = { "bib", "markdown", "org", "plaintext", "rst", "rnoweb", "tex", "rmd", "quarto" },
    settings = {
        ltex = {
            language = 'de-DE',
            additionalRules = {
                enablePickyRules = true,
                motherTongue = 'de-DE',
                languageModel = '/Users/fabianmundt/ltex/ngram-de', -- ngram-data have to be downloaded manually!
            },
        }
    }
}

-- Setup LSP Diagnostics
vim.diagnostic.config({
    virtual_text = false,
    virtual_lines = false,
    signs = true,
    underline = true,
})

-- Handle PHP Intelephense completion bug
vim.api.nvim_create_autocmd(
    'FileType', 
    {
        pattern = 'php',
        command = 'setlocal iskeyword+=$'
    }
)

-- Enable Lightbulb
vim.fn.sign_define("LightBulbSign", {text = "",  texthl = "LspLightBulbSign", linehl = "", numhl = ""})

-- Disable Logs
vim.lsp.set_log_level("off")
