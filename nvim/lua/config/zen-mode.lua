require("zen-mode").setup {
    window = {
        backdrop = 1,
        width = 110,
        height = 0.8,
        options = {
            signcolumn = "no",
            number = false,
        },
    },
    plugins = {
        gitsigns = {
            enabled = false
        },
        options = {
            enabled = true,
            ruler = false,
            showcmd = false,
            laststatus = 0,
        },
    },
    on_open = function(win)
        vim.api.nvim_set_option('scrolloff', 9999)
    end,
    on_close = function()
        vim.api.nvim_set_option('scrolloff', 15)
    end,
}

require("twilight").setup({
  context = 0,
  expand = {
    -- markdown
    "paragraph",
    "fenced_code_block",
    "list",
  }
})
