local home = os.getenv('HOME')

require('dashboard').setup {
    theme = 'doom',
    config = {
        header  = {
            '',
            '',
            '',
            '    ▐▒▒    ',
            '   ▒░▌░▌   ',
            '  ░░░▌░█▌  ',
            ' ▐░░ █░█▒  ',
            ' ░░░░░░█▒▌ ',
            '▐░░░▀   ▀▒ ',
            '▒░▀      ▀ ',
            '▒          ',
            '',
            ''
        },
        center = {
            {
                icon = " ",
                desc = "Recently Used Files",
                action = ":lua require'telescope.builtin'.oldfiles()",
                key = "o"
            },
            {
                icon = " ",
                desc = "Find repos",
                action = "Telescope repo list",
                key = "r"
            },
            {
                icon = " ",
                desc = "Find files",
                action = "Telescope find_files",
                key = "f"
            },
            {
                icon = " ",
                desc = "Settings",
                action = "tabedit $MYVIMRC",
                key = "s"
            },
            {
                icon = " ",
                desc = "Plugins",
                action = "tabedit ~/Repositories/dotfiles/nvim/lua/plugins.lua",
                key = "p"
            },
            {
                icon = " ",
                desc = "Update plugins",
                action = ":Lazy sync",
                key = "u"
            },
            {
                icon = " ",
                desc = "Close",
                action = "qa!",
                key = "q"
            },
        },
        footer  = {},
    }
}
