local background = vim.opt.background:get()
local dracula_pro = require('lualine-themes.dracula_pro_' .. background)

require('lualine').setup {
    options = {
        theme = dracula_pro,
        section_separators = {''},
        component_separators = {'|', '|'},
        disabled_filetypes = {},
        icons_enabled = true,
    },
    sections = {
        lualine_a = { {'mode', upper = true} },
        lualine_b = { {'branch', icon = ''} },
        lualine_c = {
            {'filename', file_status = true},
            'lsp-status'
        },
        lualine_x = { {'diagnostics',
            sources = {'nvim_diagnostic'},
            color_error = '#ff5555',
            color_warn = '#ffb86c',
            color_info = '#8be9fd',
            symbols = {error = ' ', warn = ' ', info = ' '}
        }, 'encoding', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location'  },
    },
}
