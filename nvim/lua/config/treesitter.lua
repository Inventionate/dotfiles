require('nvim-treesitter.configs').setup {
    ensure_installed = {
        "bash",
        "css",
        "html",
        "javascript",
        "json",
        "lua",
        "php",
        "r",
        "regex",
        "markdown",
        "markdown_inline",
        "vim",
        "vimdoc",
        "yaml",
    },
    indent = {
        enable = true
    },
    highlight = {
        enable = true
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn", -- set to `false` to disable one of the mappings
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        },
    },
    -- Select CodeChunk Code
    textobjects = {
        select = {
            enable = true,
            lookahead = true,
            keymaps = {
                ["ic"] = "@block.inner",
            },
        },
    },
}
