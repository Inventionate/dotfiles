#!/bin/sh

# Install software
brew install --cask mactex-no-gui
brew install --cask bibdesk
brew install --cask skim
brew install --cask handbrake
brew install --cask iterm2
brew install --cask rstudio
brew install --cask nextcloud
brew install --cask qlmarkdown
brew install --cask quicklook-csv
brew install --cask iina
brew install --cask r
brew install --cask sequel-ace
brew install --cask quarto
brew install --cask aldente
brew install --cask lulu
brew install --cask blockblock

# Install dependencies
brew install zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
brew install exa
brew install python
brew install pipx
brew install npm
brew install gpg2
brew install pinentry-mac
brew install ripgrep
brew install pandoc
brew install pandoc-crossref
brew install neovim
brew install starship
brew install trash-cli
brew install nextdns/tap/nextdns
brew install ffmpeg
brew install git
brew install autossh
brew install tree
brew install fd
brew install optipng
brew install languagetool
brew install luarocks
brew install mas
brew install grex
brew install --cask font-fira-code-nerd-font
brew install --cask font-vollkorn
brew install --cask font-fira-sans
brew install --cask font-fontawesome

# Install Neovim packages
npm install -g neovim
pipx install pynvim
pipx install neovim-remote
pipx install -U radian

# Create softlinks
ln -s ~/Library/Mobile\ Documents/com\~apple\~CloudDocs ~/iCloud
mkdir ~/.config/nvim
ln -s ~/repositories/dotfiles/nvim ~/.config/nvim
rm ~/.zshrc
ln -s ~/Repositories/dotfiles/.zshrc ~/.zshrc
ln -s ~/Repositories/dotfiles/starship.toml ~/.config/starship.toml
ln -s ~/iCloud/Vim/spell ~/.config/nvim/spell
ln -s ~/Repositories/dotfiles/.Rprofile ~/.Rprofile
ln -s ~/Repositories/dotfiles/TypeInfo.plist ~/Library/Application\ Support/BibDesk/TypeInfo.plist
ln -s ~/Repositories/dotfiles/.radian_profile ~/.radian_profile
ln -s ~/Repositories/dotfiles/.lintr ~/.lintr
ln -s ~/Repositories/dotfiles/gpg-agent.conf ~/.gnupg/gpg-agent.conf

# Prefs
git config --global user.name "Fabian Mundt"
git config --global user.email "f.mundt@inventionate.de"
git config --global commit.gpgsign true
git config --global core.excludesfile ~/Repositories/dotfiles/.gitignore_global

# R Packages
R -e "install.packages('lintr', repos='http://cran.us.r-project.org')"
R -e "install.packages('tidyverse', repos='http://cran.us.r-project.org')"
R -e "install.packages('gert', repos='http://cran.us.r-project.org')"
